import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import itertools
from imblearn.over_sampling import SMOTE
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import train_test_split
from sklearn.metrics import roc_curve, auc

def get_set():
    credit_cards=pd.read_csv('../dataset/creditcard.csv')
    columns=credit_cards.columns
    features_columns=columns.delete(len(columns)-1)
    x=credit_cards[features_columns]
    y=credit_cards['Class']
    return train_test_split(x, y, test_size=0.2, random_state=0)

def oversample(x,y):
    # Usamos SMOTE (Synthetic Minority Over Sampling Technique) para generar mas casos de fraude
    sampler=SMOTE(random_state=0)
    new_x,new_y=sampler.fit_sample(x,y)
    return new_x, new_y

def create_forest(x,y):
    forest = RandomForestClassifier(random_state=0)
    forest.fit(x,y)
    return forest

def test_forest(forest,x,y):
    result = forest.predict(x)
    return confusion_matrix(y,result)

def plot_confusion_matrix(cm, normalize=False, title='Confusion matrix', cmap=plt.cm.Blues):
    classes = [0,1]
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=0)
    plt.yticks(tick_marks, classes)
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
    else:
        1#print('Confusion matrix, without normalization')

    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, cm[i, j],
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')

def plot_confusion(confusion):
    plt.figure()
    plot_confusion_matrix(confusion, title='Confusion matrix')
    plt.show()

def get_test_sets():
    credit_cards=pd.read_csv('../dataset/creditcard-test.csv')
    columns=credit_cards.columns
    features_columns=columns.delete(len(columns)-1)
    x=credit_cards[features_columns]
    y=credit_cards['Class']
    return x,y

def print_results(name,forest,x,y):
    confusion = test_forest(forest,x,y)
    print ("================== "+name+" ==================")
    print ("================== RESULTADOS ==================")
    print ("CONFUSION MATRIX ")
    print (confusion)
    tp = confusion[0][0]
    fp = confusion[1][0]
    fn = confusion[0][1]
    tn = confusion[1][1]
    print( "PRECISION : ", tp / float(tp+fp))
    print( "RECALL : ", tp / float(tp+fn))
    plot_confusion(confusion)

def main():
    x_train, x_test, y_train, y_test = get_set()
    x,y = get_test_sets()
    new_x ,new_y = oversample(x_train,y_train)
    forest = create_forest(new_x,new_y)
    print_results("Todo el set",forest,x_test,y_test)
    print_results("Set con 0.18%",forest,x,y)

if __name__ == '__main__':
    main()
