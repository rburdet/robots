import csv
import sys

trainingPercentage = 66

'''
filepath: Dataset a reducir
percentage: porcentaje de documento para validaciones. DEFAULT = 99%
'''

def main(filepath,percentage):
    n = len(open(filepath).readlines())
    n = n * (percentage * 1.0 / 100.0)
    with open(filepath,'rb') as csvfile:
        with open(filepath+'training','wb') as training:
            with open(filepath + 'validation', 'wb') as validation:
                counter = 0
                writerT = csv.writer(training)
                writerV = csv.writer(validation)
                reader = csv.reader(csvfile)
                for row in reader:
                    counter += 1
                    if (counter <= n):
                        writerT.writerow(row)
                    else:
                        writerV.writerow(row)


if __name__ == '__main__':
    perc = trainingPercentage if len(sys.argv) == 2 else float(sys.argv[2])
    main(sys.argv[1],perc)
