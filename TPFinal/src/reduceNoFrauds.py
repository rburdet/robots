import csv
import sys

percentage = 99

'''
filepath: Dataset a reducir
percentage: porcentaje de no fraudes a sacar. DEFAULT = 99%
'''

def main(filepath,percentage):
    n = len(open(filepath).readlines())
    n = n * (100 - percentage)  / 100
    with open(filepath,'rb') as csvfile:
        with open(filepath+'reduced','wb') as output:
            counter = 0
            writer = csv.writer(output)
            reader = csv.reader(csvfile)
            for row in reader:
                counter += 1
                if row[-1] == '1':
                    writer.writerow(row)
                else:
                    if counter <= n:
                        writer.writerow(row)


if __name__ == '__main__':
    perc = percentage if len(sys.argv) == 2 else float(sys.argv[2])
    main(sys.argv[1],perc)
