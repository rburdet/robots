# Trabajo práctico 1

## Requirimientos
Este proyecto fue realizado y probado en un entorno con las siguientes características:
*   Java 1.8 +
*   Maven 3.5.0 +
*   Neuroph 2.8

Versiones anteriores no fueron probadas.

## Instalación

Para instalar el proyecto:
```
mvn clean install
```

Para correrlo :
```
mvn exec:java -Dexec.mainClass="com.rburdet.App" -Dexec.args="[Neuronas capa oculta]"
```

## Salidas

Un ejemplo de las salidas de este proyecto para la siguiente ejecución
```
mvn clean install && mvn exec:java -Dexec.mainClass="com.rburdet.App" -Dexec.args="30"
```

Se ven en la siguiente imagen:
![salida1](informe/salida1.PNG)


A su vez se generan archivos de comparación de entradas con salidas esperadas *testerroroutput.txt* y *SOMtesterroroutput.txt* para cada una de las redes
## Estructura

### Red Perceptrón Multicapa

La red se generó con los siguientes parámetros:
*   Capas ocultas: 1
*   Neuronas entrada : 784
*   Neuronas capa oculta : 30 (variable con la ejecución)
*   Neuronas salida : 10
*   Maximas iteraciones = 1000
*   Learning rate = 0.2
*   Máximo error posible = 0.01;

Como se ve en la [salida de ejemplo](salida1) para esta red se obtiene:
*   Error total: 0.00905
*   Iteraciones: 16
*   Éxito: 87.2%

### Red Kohonen (SOM)

La red se generó con los siguientes parámetros:

*   Neuronas entrada : 784
*   Neuronas salida : 10

Como se ve en la [salida de ejemplo](salida1) para esta red se obtiene:
*   Iteraciones: 99
*   Éxito: 9.6%

## Conclusiones
Con este simple proyecto se puede ver como una red tipo back propagation al contar con más información es más eficiente que una red no supervisada como lo es una red Kohonen. 


## Transformación del dataset

El dataset tal como fue presentado presentaba ciertas desventajas en su trabajo con neuroph. Por lo tanto se implementaron 2 scripts en python para modificar dichos archivos. El primer script encargado de normalizar los valores de las celdas tomaba el csv y lo pasaba a otro con valores de entre 0 a 1.
```
import csv
import sys

def normalize(row):
    normalizedRow = []
    minValue = 0
    maxValue = 256
    for value in row:
        result = (int(value) - minValue) / float((maxValue - minValue))
        normalizedRow.append(result)
    return normalizedRow

f = open(sys.argv[1], 'rt')
fsalida = open('out-'+sys.argv[1], 'wt')
try:
    reader = csv.reader(f)
    writer = csv.writer(fsalida,lineterminator='\n')
    for r in reader:
        writer.writerow(normalize(r))

finally:
    f.close()
    fsalida.close()
```

Además la salida se presentaba como un solo dígito del 0 al 9, lo que no era conveniente para adaptarlo a las 10 neuronas de salida. Aquí entra el juego el otro script, que pasa de un valor de 0 a 9 a un array de 10 posiciones con un 1 en el valor indicado, de esta manera el número 1 es representado por [0 1 0 0 0 0 0 0 0 0] mientras que el número 8 es [0 0 0 0 0 0 0 0 1 0] etc.

```
import csv
import sys

def convert(index):
    zeros = [0,0,0,0,0,0,0,0,0,0]
    zeros[index] = 1
    return tuple(zeros)

f = open(sys.argv[1], 'rt')
fsalida = open('out-'+sys.argv[1], 'wt')
try:
    reader = csv.reader(f)
    writer = csv.writer(fsalida,lineterminator='\n')
    for r in reader:
        writer.writerow(convert(int(r[0])))
finally:
    f.close()
    fsalida.close()
```
