package com.rburdet;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.stream.*;

import org.neuroph.core.NeuralNetwork;
import org.neuroph.core.data.DataSet;
import org.neuroph.core.data.DataSetRow;
import org.neuroph.core.events.LearningEvent;
import org.neuroph.core.events.LearningEventListener;
import org.neuroph.core.learning.LearningRule;
import org.neuroph.core.learning.SupervisedLearning;
import org.neuroph.nnet.learning.BackPropagation;
import org.neuroph.nnet.learning.KohonenLearning;
import org.neuroph.nnet.Kohonen;
import org.neuroph.nnet.MultiLayerPerceptron;


public class NumbersRecognitionNeuralNetwork {

    private static final String TRAINED_NETWORK_PATH = "bpNumberDetector.nnet";
    private static final String SOM_TRAINED_NETWORK_PATH = "SOMNumberDetector.nnet";


	private NeuralNetwork<BackPropagation> backPropagation;
	private Kohonen som;

	private Holder holder;
    
	
	public NeuralNetwork<BackPropagation> getBP(){
    	return this.backPropagation;
    }
    
    public Kohonen getSOM(){
    	return this.som;
    }
    


    public NumbersRecognitionNeuralNetwork(int in,int hidden, int out){
        this.backPropagation = new MultiLayerPerceptron(in,hidden,out);
        this.som = new Kohonen(in, out);
        this.holder = new Holder();

    }
    
    public Holder getHolder(){
    	return this.holder;
    }
    
    public void trainBp(DataSet dataset, final FileWriter fileWriter){
        int maxIterations = 1000;
        double learningRate = 0.2;
        double maxError = 0.01;
        SupervisedLearning learningRule = this.backPropagation.getLearningRule();
        learningRule.setMaxError(maxError);
        learningRule.setMaxIterations(maxIterations);
        learningRule.setLearningRate(learningRate);
        learningRule.addListener(new LearningEventListener(){
            public void handleLearningEvent(LearningEvent learningEvent){
            	SupervisedLearning  rule = (SupervisedLearning) learningEvent.getSource();
            	writeErrorToFile(fileWriter,"Network error for interation " + rule.getCurrentIteration() + ": "+ rule.getTotalNetworkError());
            	holder.maxIt = rule.getCurrentIteration();
            	holder.error = rule.getTotalNetworkError();
    		}
        });
        this.backPropagation.learn(dataset);
        this.backPropagation.save(TRAINED_NETWORK_PATH);
    }

	public void getTrainDistribution(NeuralNetwork nn, DataSet dataset){
		int[] outputs = getDistribution(nn,dataset);
		System.out.println("La distribucion del set de training fue: "+Arrays.toString(outputs));
	}

    public void testNN(NeuralNetwork nn, DataSet dataset, final FileWriter fileWriter){
    	for(DataSetRow testSetRow : dataset.getRows()) {
            nn.setInput(testSetRow.getInput());
            nn.calculate();
            double[] networkOutput = nn.getOutput();
            writeErrorToFile(fileWriter, "Input: " + Arrays.toString( testSetRow.getInput() ));
            writeErrorToFile(fileWriter, " Output: " + Arrays.toString( networkOutput));
        }
		int[] outputs = getDistribution(nn,dataset);
		System.out.println("Los hits se distribuyeron de la siguiente manera: " + Arrays.toString(outputs));
		System.out.println("Hubo: " + 100 * (double)IntStream.of(outputs).sum() / dataset.size() + " % De aciertos");
    }
	
    
    public void trainSom(DataSet dataset){
    	this.som.learn(dataset);
    	KohonenLearning learningRule = (KohonenLearning) this.som.getLearningRule();
    	System.out.println("Iteraciones para SOM: "+learningRule.getIteration());
        this.som.save(SOM_TRAINED_NETWORK_PATH);

    }
	
    public void writeErrorToFile(FileWriter fileWriter,String text){
	    BufferedWriter bw = new BufferedWriter(fileWriter);
	    try {
			bw.write(text);
			bw.newLine();
			bw.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	    
    }

	public int getPredictedNumber(double[] desiredOut,double[] out){
		double max = 0.0;
		int idx = 0;
		for (int i = 0 ; i < out.length ; i++){
			if (out[i] > max){
				max = out[i];
				idx = i;
			}
		}
		if (desiredOut[idx] == 1.0){
			return idx;
		}
		return -1;
	}

	public int[] getDistribution(NeuralNetwork nn, DataSet dataset){
		int[] distribution = new int[10];
		for(DataSetRow row : dataset.getRows()) {
			nn.setInput(row.getInput());
			nn.calculate();
			double[] output = nn.getOutput();
			int idx = getPredictedNumber(row.getDesiredOutput(), output);
			if (idx != -1)
				distribution[idx] += 1;
		}
		return distribution;
	}


    static class Holder{ 
        public int maxIt;
        public double error;
    }
	
}
