package com.rburdet;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;

import org.neuroph.core.data.DataSet;



/**
 * Hello world!
 *
 */
public class App{
    private final static String TRAINING_FILE = "input/finalset.csv";
    private final static String TEST_FILE = "input/finaltestset.csv";
 
	private static final String ERROR_FILE = "erroroutput.txt";
	private static final String TEST_ERROR_FILE = "testerroroutput.txt";
	private static final String TEST_ERROR_SOM_FILE = "SOMtesterroroutput.txt";
    private static final int INPUT_NEURONS = 784;
    private static final int OUTPUT_NEURONS = 10;


    public static void main( String[] args ){
        FileWriter fileErrorTrainWriter = null;
        FileWriter fileErrorTestWriter = null;
        FileWriter fileErrorTestWriterSOM = null;
        try{
	        fileErrorTrainWriter = new FileWriter(ERROR_FILE);
	        fileErrorTestWriter = new FileWriter(TEST_ERROR_FILE);
	        fileErrorTestWriterSOM = new FileWriter(TEST_ERROR_SOM_FILE);
	        
	    }catch (IOException ex){
	    	 System.out.println("Error abriendo el archivo");
        }
        int hidden = checkArgs(args);
        
        NumbersRecognitionNeuralNetwork nn = new NumbersRecognitionNeuralNetwork(INPUT_NEURONS, hidden, OUTPUT_NEURONS);
        System.out.println("Empezando entrenamiento");
		DataSet trainingDataSet = DataSet.createFromFile(TRAINING_FILE,INPUT_NEURONS,OUTPUT_NEURONS,",",true);
        nn.trainBp(trainingDataSet,fileErrorTrainWriter);
        System.out.println("Entrenamiento terminado");
		nn.getTrainDistribution(nn.getBP(), trainingDataSet);
        System.out.println("Error BP: "+nn.getHolder().error);
        System.out.println("Iteraciones BP: "+nn.getHolder().maxIt);
        
        System.out.println("Empezando pruebas");
        nn.testNN(nn.getBP(),DataSet.createFromFile(TEST_FILE,INPUT_NEURONS,OUTPUT_NEURONS,",",true),fileErrorTestWriter);
        System.out.println("Pruebas terminadas ver "+TEST_ERROR_FILE);

        System.out.println("Empezando entrenamiento SOM");
        nn.trainSom(DataSet.createFromFile(TRAINING_FILE,INPUT_NEURONS,OUTPUT_NEURONS,",",true));
        System.out.println("Entrenamiento SOM Terminado");
		nn.getTrainDistribution(nn.getBP(), trainingDataSet);
        System.out.println("Empezando pruebas SOM");
        nn.testNN(nn.getSOM(),DataSet.createFromFile(TEST_FILE,INPUT_NEURONS,OUTPUT_NEURONS,",",true),fileErrorTestWriterSOM);
        System.out.println("Pruebas terminadas ver "+TEST_ERROR_SOM_FILE);

        closeFile(fileErrorTestWriter);
        closeFile(fileErrorTrainWriter);
        closeFile(fileErrorTestWriterSOM);

    }


    
    public static void closeFile(FileWriter file){
	    if (file!= null){
	        try {
				file.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
	    }
    }
    
	private static int checkArgs(String[] args) {
		int hidden = 0;
		if (args.length > 0) {
            try {
                hidden = Integer.parseInt(args[0]);
                System.out.println("Hidden neurons: "+ String.valueOf(hidden));
            } catch (NumberFormatException e) {
                System.err.println("El primer argumento es la cantidad de neuronas de la capa oculta " + args[0] + " Tiene que ser un entero");
                System.exit(1);
            }
        }
		return hidden;
	}
}


