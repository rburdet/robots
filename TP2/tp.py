import sys
import math
import random
from dibujador import create_board

TAMANO = 12
OBJ = 12
CANTIDAD_HIJOS = 10 # Fijo, siempre pasan 10
CANTIDAD_MUTACIONES_MAX = 4 # Mutaciones: [0,4)
POBLACION_INICIAL_MAX = 50 # Mutaciones: [1,50]
PROBABILIDAD_MUTACION = 0.1
CANTIDAD_MAX_GENERACIONES = 100000000

class Cromosoma:
    def __init__(self, tamano, reinas = None, mezclar = True):
        self.tamano = tamano
        self.aptitud = 0
        if reinas is None:
            self.reinas = list(range(self.tamano))
        if mezclar:
            self.mezclar(random.randrange(self.tamano))

    def set_reinas(self,reinas):
        self.reinas = reinas


    def mezclar(self,veces):
        for i in range(veces):
            j = random.randrange(self.tamano)
            k = random.randrange(self.tamano)
            aux = self.reinas[j]
            self.reinas[j] = self.reinas[k]
            self.reinas[k] = aux
        #self.calcular_aptitud()

    def calcular_aptitud(self):
        self.aptitud = 0
        for i in range(self.tamano):
            come = False
            for j in range(i+1, self.tamano):
                # comparo la reina x con la reina y
                colx = i
                coly = j
                filax = self.reinas[i]
                filay = self.reinas[j]
                if (math.fabs(filax - filay) == coly - colx or filax - filay == 0):
                    come = True
                    break
            if not come:
                self.aptitud += 1
            else:
                self.aptitud = 0
                break

    def mutar_individuo(self):
        mutacionesMax = random.randrange(CANTIDAD_MUTACIONES_MAX)
        if random.uniform(0,1) < PROBABILIDAD_MUTACION:
            self.mezclar(mutacionesMax)

def mutar_poblacion(poblacion):
    for cromosoma in poblacion:
        cromosoma.mutar_individuo()
        #cromosoma.calcular_aptitud()
    return poblacion


def generar_poblacion(tamano):
    cant_indviduos = random.randrange(POBLACION_INICIAL_MAX)+1
    poblacion = []
    for i in range(cant_indviduos):
        poblacion.append(Cromosoma(tamano))
    return poblacion

    # Cruza 3 puntos
def cruzar(poblacion):
    for i in range(0,len(poblacion)-1,2):
        reinas1 , reinas2 = poblacion[i].reinas , poblacion[i+1].reinas
        chunks1,chunks2,aux1,aux2 = [],[],[],[]
        for j in range(0, len(reinas1), 4):
            chunks1.append(reinas1[j:j + 4])
            chunks2.append(reinas2[j:j + 4])
        aux1 = chunks1[0] + chunks2[1] + chunks1[2]
        aux2 = chunks2[0] + chunks1[1] + chunks2[2]
        poblacion[i].set_reinas(aux1)
        poblacion[i+1].set_reinas(aux2)
    return poblacion

# Seleccion por ruleta
def seleccionar(poblacion):
    new_pob = []
    aptitud_total = sum(cromosoma.aptitud for cromosoma in poblacion)
    rand = random.uniform(0,aptitud_total)
    actual = 0
    for i, cromosoma in enumerate(poblacion):
        actual += cromosoma.aptitud
        if actual >= rand:
            new_pob.append(cromosoma)
        if len(new_pob) >= CANTIDAD_HIJOS:
            break
    return new_pob

def obtener_mejor(poblacion):
    max = 0
    resultado = poblacion[0]
    for cromosoma in poblacion:
        cromosoma.calcular_aptitud()
        if cromosoma.aptitud > max:
            max = cromosoma.aptitud
            resultado = cromosoma
    return resultado


def ga(tamano):
    # Algoritmo genetico
    poblacion = generar_poblacion(tamano)
    mejor = poblacion[0]
    for i in range(CANTIDAD_MAX_GENERACIONES):
        print("Gen ",i)
        objetivo = obtener_mejor(poblacion)
        if objetivo.aptitud > mejor.aptitud:
            mejor = objetivo
            if mejor.aptitud == OBJ:
                return objetivo
        poblacion = seleccionar(poblacion)
        #poblacion = cruzar(poblacion)
        poblacion = mutar_poblacion(poblacion)
    return mejor




def main():
    tamano = sys.argv[1] if len(sys.argv) > 1 else TAMANO
    solucion = ga(tamano)
    create_board(tamano,solucion.reinas)

if __name__ == '__main__':
    main()
